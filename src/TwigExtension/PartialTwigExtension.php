<?php

namespace Drupal\partial\TwigExtension;

/**
 * PartialTwigExtension class.
 */
class PartialTwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'partial';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return array(
      new \Twig_SimpleFunction('partial', array($this, 'partialFunction'), array(
        'name' => NULL,
        'context' => [],
      )),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return array(
      new \Twig_SimpleFilter('partial', array($this, 'partialfilter'), array(
        'name' => NULL,
      )),
    );
  }

  /**
   * Render partial.
   */
  public function partialFunction($name, $context = array()) {
    $build['partial'] = [
      '#theme' => 'partial',
      '#name' => $name,
      '#context' => $context,
    ];
    return render($build);
  }

  /**
   * Render partial as a Twig Filter.
   */
  public function partialFilter($context = array(), $name = NULL) {
    $build['partial'] = [
      '#theme' => 'partial',
      '#name' => $name,
      '#context' => $context,
    ];
    return render($build);
  }

}
